#include <Constants.au3>
#include "IE_v1.au3"

If $CmdLine[0] < 2 Then
   MsgBox($MB_OK, "Usage", "ps_nutanix <username> <password>")
Else
   prism_login($CmdLine[1], $CmdLine[2])
EndIf

;########################################################################
;This section is the start of the function call
;Where the IE browser is launched and the target URL is navigated to
;########################################################################
Func prism_login($username, $password)
   Local $url = "https://10.16.1.45:9440/console/#login"
   Local $oIE = _IECreate($url)

   $hwnd = _IEPropertyGet($oIE, "hwnd")
   WinActivate($hwnd)
   WinSetState($hwnd, "", @SW_MAXIMIZE)
   Sleep(1000)

   ;##################################################################
   ;This section detects if the Prism site has an untrusted certificate
   ;It handles the certificate error users will encounter and accepts the certificate error so that the prism page can load
   ;##################################################################
   if WinWait("Certificate Error:","",1)<>0 Then
	  $sMyString = "Continue to this website (not recommended)."
	  $oLinks = _IELinkGetCollection($oIE)

	  For $oLink in $oLinks
	  $sLinkText = _IEPropertyGet($oLink, "innerText")
		If StringInStr($sLinkText, $sMyString) Then
		  _IEAction($oLink, "click")
		  $oLinks = _IELinkGetCollection($oIE)
			;MsgBox($MB_OK,"title","clicked 1")
			ExitLoop
		EndIf
	  next
   EndIf
   ;#####################################################################
   ;This section is the web behavior of the Prism Screen.
   ;The functions mimic that of a user entering the username and password
   ;#####################################################################
   Sleep(2000)
   WinWaitActive("Nutanix Web Console","",300)
	 ; _IELoadWait($url, 0, 2000)
   Sleep(2000)
   Send($username&"@nutanixbd.local")
   Sleep(2000)
   Send("{Tab}")
   Sleep(2000)
   Send($password)
   Sleep(2000)
   Send("{Enter}")
   Sleep(500)
WinWaitClose($hwnd)
EndFunc ;==>prism_login()