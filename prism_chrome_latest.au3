#include "wd_core.au3"
#include "wd_helper.au3"

Local $sDesiredCapabilities, $sSession

SetupChrome()

If $CmdLine[0] < 2 Then
   MsgBox($MB_OK, "Usage", "ps_nutanix <username> <password>")
Else
   prism_login($CmdLine[1], $CmdLine[2])
EndIf

;prism_login("admin", "Nutanix/4u")

Func prism_login($Login, $Pass)
   $PE = "https://10.16.0.215:9440"
   _WD_Startup()
   $sSession = _WD_CreateSession($sDesiredCapabilities)
   _WD_Navigate($sSession, $PE)
   sleep(2000)
   $Login=$Login&"@nutanixbd.local"
   _ChromeSetInputValueById($sSession, 'inputUsername', $Login)
   _ChromeSetInputValueById($sSession, 'inputPassword', $Pass)
   _ChromeClickElementByClass($sSession, 'loginSubmit')
EndFunc

Func prism_logout($Login, $Pass)
   sleep(5000)
   _ChromeClickElementByClass($sSession, 'n-username')
   sleep(2000)
   _ChromeClickElementByDataTest($sSession, 'user_menu_sign_out')
   sleep(3000)
   _WD_DeleteSession($sSession)
   _WD_Shutdown()
EndFunc


Func SetupChrome()
   _WD_Option('Driver', 'chromedriver.exe')
   _WD_Option('Port', 9515)
   _WD_Option('DriverParams', '--log-path="' & @ScriptDir & '\chrome.log"')


   $sDesiredCapabilities = '{"capabilities": {"alwaysMatch": {"goog:chromeOptions": {"w3c": true }}}}'
EndFunc

Func SetupFirefox()
   _WD_Option('Driver', 'geckodriver.exe')
   ;_WD_Option('DriverParams', '--log trace')
   _WD_Option('DriverParams', '-p 1234')
   _WD_Option('Port', 1234)
   ;_WD_Option('DriverParams', '--log-path="' & @ScriptDir & '\chrome.log"')
   $sDesiredCapabilities = '{"desiredCapabilities":{"javascriptEnabled":true,"nativeEvents":true,"acceptInsecureCerts":true}}'
   ;$sDesiredCapabilities = ''
EndFunc

Func _ChromeSetInputValueById($sSession, $Id, $Value)
   $sButton = _WD_FindElement($sSession, $_WD_LOCATOR_ByXPath, "//input[@id='"&$Id&"']")
   _WD_ElementAction($sSession, $sButton, 'value', $Value)
EndFunc

Func _ChromeSetInputValueByName($sSession, $Name, $Value)
   $sButton = _WD_FindElement($sSession, $_WD_LOCATOR_ByXPath, "//*[@name='"&$Name&"']")
   ConsoleWrite("//*[@name='"&$Name&"']")
   _WD_ElementAction($sSession, $sButton, 'value', $Value)
EndFunc


Func _ChromeClickElementById($sSession, $Id)
   $sButton = _WD_FindElement($sSession, $_WD_LOCATOR_ByXPath, "//input[@id='"&$Id&"']")
   _WD_ElementAction($sSession, $sButton, 'click')
EndFunc

Func _ChromeClickElementByClass($sSession, $Class)
   $sButton = _WD_FindElement($sSession, $_WD_LOCATOR_ByXPath, "//*[@class='"&$Class&"']")
   _WD_ElementAction($sSession, $sButton, 'click')
EndFunc

Func _ChromeClickElementByDataTest($sSession, $DataTest)
   $sButton = _WD_FindElement($sSession, $_WD_LOCATOR_ByXPath, "//*[@data-test='"&$DataTest&"']")
   _WD_ElementAction($sSession, $sButton, 'click')
EndFunc